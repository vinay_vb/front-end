import {Component, OnInit} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Opportunity} from "./opportunity.model";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  targetList: Opportunity[] = new Array();

  constructor(private httpClient: HttpClient) {}

  ngOnInit(): void {
    this.fetchDataFromServer("new");
  }

  fetchDataFromServer(status: string) {
    return this.httpClient.get<Opportunity[]>('/opportunities/users/5/opportunities', {
      params: {
        'status': status
      }
    })
        .subscribe((opportunities: Opportunity[]) => {
          this.targetList = opportunities;
        })
  }
}
